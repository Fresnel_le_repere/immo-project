

**Bootstrap 4 Support**
Now UI Kit is built on top of the much awaited Bootstrap 4. This makes starting a new project very simple. It also provides benefits if you are already working on a Bootstrap 4 project; you can just import the Now UI Kit style over it. Most of the elements have been redesigned; but if you are using an element we have not touched, it will fall back to the Bootstrap default.

**Example Pages**
1//03LQIWQEqHh69CgYIARAAGAMSNwF-L9IrB6QGp7EWeogqXO7JwkaES8yOn2isumLU-13mVJOZaWjFX45RSZ9qCQ2chpB2YphGkRU
We wanted to fully display the power of this kit, so the kit comes packed with examples showing you how to use the components. Inside the product you will find:

## Links:

+ [Live Preview](https://creativetimofficial.github.io/now-ui-kit-angular/index)
+ [Landing Page](https://creativetimofficial.github.io/now-ui-kit-angular/examples/landing)
+ [Login Page](https://creativetimofficial.github.io/now-ui-kit-angular/examples/login)
+ [Profile Page](https://creativetimofficial.github.io/now-ui-kit-angular/examples/profile)
+ [Now UI Kit PRO Angular](https://creativetimofficial.github.io/now-ui-kit-pro-angular/presentation) (from $79)


## Terminal Commands

1. Install NodeJs from [NodeJs Official Page](https://nodejs.org/en).
2. Open Terminal
3. Go to your file project
4. Run in terminal: ```npm install -g @angular/cli```
5. Then: ```npm install```
6. And: ```npm start```
7. Navigate to [localhost:4200](localhost:4200)
### What's included

Within the download you'll find the following directories and files:

```
Now Ui Kit Angular
├── CHANGELOG.md
├── README.md
├── angular.json
├── documentation
│   ├── css
│   ├── js
│   └── tutorial-components.html
├── e2e
├── karma.conf.js
├── package-lock.json
├── package.json
├── protractor.conf.js
├── src
│   ├── app
│   │   ├── app.component.html
│   │   ├── app.component.scss
│   │   ├── app.component.spec.ts
│   │   ├── app.component.ts
│   │   ├── app.module.ts
│   │   ├── app.routing.ts
│   │   ├── components
│   │   │   ├── basicelements
│   │   │   │   ├── basicelements.component.html
│   │   │   │   ├── basicelements.component.scss
│   │   │   │   ├── basicelements.component.spec.ts
│   │   │   │   └── basicelements.component.ts
│   │   │   ├── components.component.html
│   │   │   ├── components.component.ts
│   │   │   ├── components.module.ts
│   │   │   ├── modal
│   │   │   │   ├── modal.component.html
│   │   │   │   ├── modal.component.scss
│   │   │   │   ├── modal.component.spec.ts
│   │   │   │   └── modal.component.ts
│   │   │   ├── navigation
│   │   │   │   ├── navigation.component.html
│   │   │   │   ├── navigation.component.scss
│   │   │   │   ├── navigation.component.spec.ts
│   │   │   │   └── navigation.component.ts
│   │   │   ├── notification
│   │   │   │   ├── notification.component.html
│   │   │   │   ├── notification.component.scss
│   │   │   │   ├── notification.component.spec.ts
│   │   │   │   └── notification.component.ts
│   │   │   ├── nucleoicons
│   │   │   │   ├── nucleoicons.component.html
│   │   │   │   ├── nucleoicons.component.scss
│   │   │   │   ├── nucleoicons.component.spec.ts
│   │   │   │   └── nucleoicons.component.ts
│   │   │   └── typography
│   │   │       ├── typography.component.html
│   │   │       ├── typography.component.scss
│   │   │       ├── typography.component.spec.ts
│   │   │       └── typography.component.ts
│   │   ├── examples
│   │   │   ├── examples.component.html
│   │   │   ├── examples.component.scss
│   │   │   ├── examples.component.spec.ts
│   │   │   ├── examples.component.ts
│   │   │   ├── examples.module.ts
│   │   │   ├── landing
│   │   │   │   ├── landing.component.html
│   │   │   │   ├── landing.component.scss
│   │   │   │   ├── landing.component.spec.ts
│   │   │   │   └── landing.component.ts
│   │   │   ├── login
│   │   │   │   ├── login.component.html
│   │   │   │   ├── login.component.scss
│   │   │   │   ├── login.component.spec.ts
│   │   │   │   └── login.component.ts
│   │   │   └── profile
│   │   │       ├── profile.component.html
│   │   │       ├── profile.component.scss
│   │   │       ├── profile.component.spec.ts
│   │   │       └── profile.component.ts
│   │   └── shared
│   │       └── navbar
│   │           ├── navbar.component.html
│   │           ├── navbar.component.scss
│   │           ├── navbar.component.spec.ts
│   │           └── navbar.component.ts
│   ├── assets
│   │   ├── css
│   │   │   └── demo.css
│   │   ├── fonts
│   │   ├── img
│   │   └── sass
│   │       ├── now-ui-kit
│   │       └── now-ui-kit.scss
│   ├── environments
│   ├── favicon.ico
│   ├── index.html
│   ├── main.ts
│   ├── polyfills.ts
│   ├── styles.scss
│   ├── test.ts
│   ├── tsconfig.app.json
│   ├── tsconfig.spec.json
│   └── typings.d.ts
├── tsconfig.json
└── tslint.json
```